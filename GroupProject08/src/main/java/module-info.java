module uni.gp.groupproject {
    requires javafx.controls;
    requires javafx.fxml;
    requires org.mongodb.driver.sync.client;
    requires org.mongodb.bson;
    requires org.mongodb.driver.core;


    opens uni.gp08.groupproject08 to javafx.fxml;
    exports uni.gp08.groupproject08;
    exports uni.gp08.utils;
    exports uni.gp08.enums;
    opens uni.gp08.utils to javafx.fxml;
    exports uni.gp08.types;
    opens uni.gp08.types to javafx.fxml;
}