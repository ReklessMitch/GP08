package uni.gp08.utils;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import uni.gp08.enums.Piece;
import uni.gp08.types.MoveSet;
import uni.gp08.types.Position;

import java.util.*;

public class Board {

    boolean whiteTurn = true;
    boolean kingInCheck = false;
    int turns = 0;
    List<MoveSet> moves = new ArrayList<>();
    GridPane chessBoard;
    List<int[]> possibleMoves = new ArrayList<>();
    IntegerProperty whiteScore = new SimpleIntegerProperty();
    IntegerProperty blackScore = new SimpleIntegerProperty();

    Material pieceSelected = null;
    Material enPassant = null;
    List<Material> boardPieces = new ArrayList<>();

    final int[][] horizontalDirections = {{-1, 0}, {1, 0}, {0, -1}, {0, 1}};
    final int[][] diagonalDirections = {{-1, -1}, {-1, 1}, {1, -1}, {1, 1}};
    final int[][] knightDirections = {{-2, -1}, {-2, 1}, {-1, -2}, {-1, 2}, {1, -2}, {1, 2}, {2, -1}, {2, 1}};


    public Board(GridPane chessBoard) {
        this.chessBoard = chessBoard;
    }


    public void getMoveForPiece(Material piece){
        switch (piece.getLabel()) {
            case BLACK_QUEEN, WHITE_QUEEN -> {
                getMoves(piece, diagonalDirections);
                getMoves(piece, horizontalDirections);}
            case BLACK_BISHOP, WHITE_BISHOP -> getMoves(piece, diagonalDirections);
            case BLACK_ROOK, WHITE_ROOK -> getMoves(piece, horizontalDirections);
            case BLACK_KNIGHT, WHITE_KNIGHT -> getKnightMoves(piece, knightDirections);
            case WHITE_PAWN, BLACK_PAWN -> getPawnMoves(piece);
            case BLACK_KING, WHITE_KING -> getKingMoves(piece);
            default -> throw new IllegalStateException("Unexpected value");
        }
    }

    public void setKingInCheck(){
        kingInCheck = true;
    }

    public void checkIfCheck(String team){

        // to turn this into checkmate, get the piece/s which makes it check and see if it can block that path or take that piece,
        // as well as this, see if the king can move and then check if that's also in danger for each of the moves!

        for(Material piece : boardPieces) {
            Rectangle square = (Rectangle) chessBoard.getChildren().get(piece.getRow() * 8 + piece.getColumn());
            setBackground(piece.getRow(), piece.getColumn(), square);
            if (piece.getTeam().equals(team)) {
                getMoveForPiece(piece);
            }
        }
        for(int[] move : possibleMoves){
            Material chessPiece = findChessMaterial(move[0], move[1]);
            if(chessPiece == null){continue;}
            if (chessPiece.chessPiece == Piece.BLACK_KING || chessPiece.chessPiece == Piece.WHITE_KING){
                setBorder(chessPiece.getRow(), chessPiece.getColumn(), Color.RED);
                possibleMoves.clear();
                break;
            }
        }
        possibleMoves.clear();

    }

    public void addToBlackScore(int points) {
        blackScore.set(blackScore.get() + points);
    }

    public void addToWhiteScore(int points) {
        whiteScore.set(whiteScore.get() + points);
    }

    public List<MoveSet> getMoves() {
        return moves;
    }

    public boolean isMoveValid(int row, int col){

        // Check if King can move of set colour


        if(pieceSelected != null) {
            for (int[] move : possibleMoves) {
                if (move[0] == row && move[1] == col) {
                    return true;
                }
            }
            System.out.println("Invalid Move");
            return false;
        }
        return false;
    }

    public void addScore(Material chessPiece){
        if(!chessPiece.getTeam().equals("black")){
            addToBlackScore(chessPiece.getWorth());
        }else {
            addToWhiteScore(chessPiece.getWorth());
        }
    }

    public void removePiece(Material chessPiece){
        boardPieces.remove(chessPiece);
        chessBoard.getChildren().remove(chessPiece.sceneLabel);
        addScore(chessPiece);
    }

    public void checkEnPassent(int row, int col, Material chessPiece){
        if(enPassant != null && Math.abs(pieceSelected.getRow() - row) == 1 && Math.abs(pieceSelected.getColumn() - col) == 1){
            int direction = whiteTurn ? 1 : -1;
            System.out.println("ROW: " + row + "COL: " + col + "IN EN PASS");
            chessPiece = findChessMaterial(row + direction, col);
            removePiece(chessPiece);
            System.out.println("EN PASSENT HAS BEEN DONE LOLW");
            enPassant = null;
        }

        if(pieceSelected.chessPiece.equals(Piece.WHITE_PAWN) || pieceSelected.chessPiece.equals(Piece.BLACK_PAWN) &&
                Math.abs(pieceSelected.getRow() - row) == 2){
            enPassant = pieceSelected;
            System.out.println("EN PASSENT");
        }else{enPassant = null;}
    }

    public void move(int row, int col){
        if(isMoveValid(row, col)){
            Material chessPiece = findChessMaterial(row, col);

            if(chessPiece != null) {removePiece(chessPiece);}else {checkEnPassent(row, col, chessPiece);}

            Position newPos = new Position(row, col);
            MoveSet moveSet = new MoveSet(pieceSelected, newPos);
            moves.add(moveSet);

            // Continue

            System.out.println(row + " " + col);
            pieceSelected.setRow(row);
            pieceSelected.setColumn(col);
            GridPane.setRowIndex(pieceSelected.sceneLabel, pieceSelected.position.getRow());
            GridPane.setColumnIndex(pieceSelected.sceneLabel, pieceSelected.position.getCol());
            deselectPiece(pieceSelected);
            whiteTurn = !whiteTurn;

        }
    }

    public void setBackground(int row, int col, Rectangle square){
        if ((row + col) % 2 == 0) {
            square.setFill(Color.BLUEVIOLET);
        } else {
            square.setFill(Color.SADDLEBROWN);
        }
    }

    public Material findChessMaterial(int row, int col){
        for(Material material : boardPieces){
            if(material.getRow() == row && material.getColumn() == col){
                return material;
            }
        }
        return null;
    }

    void checkPieceColour(int row, int col, String team){
        Material chessMat = findChessMaterial(row, col);
        if (chessMat == null) {possibleMoves.add(new int[]{row, col});
        }else{
            // System.out.println(chessMat);
            // System.out.println(chessMat.chessPiece + "Row:" + row + "Col: " + col );
            if(!chessMat.team.equals(team)){
                if(chessMat.chessPiece == Piece.BLACK_KING || chessMat.chessPiece == Piece.WHITE_KING){setKingInCheck();}
                possibleMoves.add(new int[]{row, col});
                System.out.println("Added?");
            }
        }
    }

    public void deselectPiece(Material material){
        material.resetColour();
        pieceSelected = null;
        for(int[] move: possibleMoves){
            System.out.println(Arrays.toString(move));
            Rectangle square = (Rectangle) chessBoard.getChildren().get(move[0] * 8 + move[1]);
            setBackground(move[0], move[1], square);
        }
        possibleMoves.clear();
        checkIfCheck(material.getTeam());
    }

    public void setPieceSelected(Material material){
        System.out.println("Setting piece:"  + material.toString());
        if(pieceSelected != null && material != pieceSelected){
            deselectPiece(pieceSelected);
            possibleMoves.clear();
        }
        this.pieceSelected = material;
        highlightValidSquares(material);
        material.selectedColour();
    }

    private void highlightValidSquares(Material material) {
        getMoveForPiece(material);
        getInside(possibleMoves);
    }

    public void getPawnMoves(Material material){
        int row = material.getRow();
        int col = material.getColumn();
        String colour = material.getTeam();

        int direction = colour.equals("white") ? -1 : 1;

        // Beginning Chess pawns moving two pieces
        if((colour.equals("white") && row == 6) || (colour.equals("black") && row == 1)){
            if(findChessMaterial(row + direction * 2, col) == null){
                possibleMoves.add(new int[]{row + direction * 2, col});
            }
        }

        // Check En Passant
        if(enPassant != null) {

            if (Math.abs(enPassant.getColumn() - col) == 1 && row == enPassant.getRow() &&
                    findChessMaterial(enPassant.getRow() + direction, enPassant.getColumn()) == null) {
                possibleMoves.add(new int[]{enPassant.getRow() + direction, enPassant.getColumn()});
                System.out.println("EN PASSENT POSSIBLE!");
            }
        }

        // Check moving forward
        if(findChessMaterial(row + direction, col) == null){possibleMoves.add(new int[]{row + direction, col});}

        // Check diagonals
        for (int i = -1; i <= 1; i += 2) {
            Material chessPiece = findChessMaterial(row + direction, col + i);
            if(chessPiece != null){
                checkPieceColour(row + direction, col + i, colour);
            }
        }
    }


    void getKingMoves(Material material) {
        int row = material.getRow();
        int col = material.getColumn();
        String team = material.getTeam();

        for (int i = row - 1; i <= row + 1; i++) {
            for (int j = col - 1; j <= col + 1; j++) {
                if (i < 0 || i > 7 || j < 0 || j > 7) continue;
                checkPieceColour(i, j, team);
            }
        }
    }

    private void getInside(List<int[]> possibleMoves){
        for(int[] move : possibleMoves){
            if(move[0] >= 0 && move[0] < 8 && move[1] >= 0 && move[1] < 8){
                setBorder(move[0], move[1], Color.GREEN);
            }
        }
    }

    void getKnightMoves(Material material, int[][] directions) {
        int row = material.getRow();
        int col = material.getColumn();
        for (int[] knightDirection : directions) {
            int newRow = row + knightDirection[0];
            int newCol = col + knightDirection[1];
            if (newRow < 0 || newRow > 7 || newCol < 0 || newCol > 7) continue;
            checkPieceColour(newRow, newCol, material.getTeam());
        }

    }



    void getMoves(Material material, int[][] directions) {
        int row = material.getRow();
        int col = material.getColumn();
        for (int[] direction : directions) {
            for (int j = 1; j < 8; j++) {
                int newRow = row + direction[0] * j;
                int newCol = col + direction[1] * j;
                if (newRow < 0 || newRow > 7 || newCol < 0 || newCol > 7) break;
                Material chessMat = findChessMaterial(newRow, newCol);

                if (chessMat == null) {
                    possibleMoves.add(new int[]{newRow, newCol});
                } else {
                    if (!chessMat.team.equals(material.getTeam())) {
                        if(chessMat.chessPiece == Piece.BLACK_KING || chessMat.chessPiece == Piece.WHITE_KING){
                            setKingInCheck();
                        }
                        possibleMoves.add(new int[]{newRow, newCol});
                    }
                    break;
                }
            }
        }

    }

    private void setBorder(int row, int col, Color color){
        Rectangle square = (Rectangle) chessBoard.getChildren().get(row * 8 + col);
        square.setStyle(null);
        square.setFill(color);
    }

}
