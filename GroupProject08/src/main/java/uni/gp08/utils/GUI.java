package uni.gp08.utils;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.binding.Bindings;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.util.Duration;
import uni.gp08.enums.Piece;
import uni.gp08.types.MoveSet;

import java.util.ArrayList;
import java.util.List;


public class GUI {
    Stage stage;
    Scene scene;

    Board board;
    GridPane chessBoard = new GridPane();
    Timeline timeline;
    VBox infoBox = new VBox();
    VBox movesBox = new VBox();
    Label timeLabel = new Label();
    Label scoreLabel = new Label();
    List<Label> moves = new ArrayList<>();
    IntegerProperty time = new SimpleIntegerProperty();


    public GUI() {
        board = new Board(chessBoard);
        stage = new Stage();
        scene = new Scene(chessBoard);
        setUpBoard();
        setUp();
        stage.setScene(scene);
        stage.setTitle("Chess Board");
    }

    public void setUpMoves(){
        for(MoveSet move: board.getMoves()){
            String material = move.getMaterialLabel().label;
            String row = String.valueOf(move.getEndPos().getRowLetter());
            String col = String.valueOf(move.getEndPos().getCol());

            Label newLabel = new Label();
            newLabel.textProperty().bind(Bindings.createStringBinding(() ->
                    String.format("%s %s%s", material, row, col), time));
            newLabel.setText(material.concat(" - ").concat(row).concat(col));
            moves.add(newLabel);
        }
        chessBoard.add(movesBox, 8, 0);
        movesBox.getChildren().addAll(moves);
    }
    public void setUp(){
        setUpTime();
        setUpMoves();
        scoreLabel.textProperty().bind(board.whiteScore.asString().concat(" - ").concat(board.blackScore.asString()));
        infoBox.getChildren().addAll(timeLabel, scoreLabel);
        chessBoard.add(infoBox, 8, 0);
    }

    public void setUpTime(){


        timeLabel.textProperty().bind(Bindings.createStringBinding(() ->
                String.format("Time: %02d:%02d", time.get() / 60, time.get() % 60), time));
        timeline = new Timeline(new KeyFrame(Duration.seconds(1), evt -> time.set(time.get() + 1)));
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();
    }

    public void addPieces(){
        for (int i = 0; i < 8; i++) {
            addPieceToBoard(Piece.WHITE_PAWN, 6, i, "white", 1);
            addPieceToBoard(Piece.BLACK_PAWN, 1, i, "black", 1);
        }
        addPieceToBoard(Piece.WHITE_KING, 7, 4, "white", 0);
        addPieceToBoard(Piece.WHITE_QUEEN, 7, 3, "white",9);
        addPieceToBoard(Piece.WHITE_BISHOP, 7, 2, "white",3);
        addPieceToBoard(Piece.WHITE_BISHOP, 7, 5, "white", 3);
        addPieceToBoard(Piece.WHITE_KNIGHT, 7, 6, "white", 3);
        addPieceToBoard(Piece.WHITE_KNIGHT, 7, 1, "white", 3);
        addPieceToBoard(Piece.WHITE_ROOK, 4, 4, "white", 5);
        addPieceToBoard(Piece.WHITE_ROOK, 7, 7, "white", 5);
        addPieceToBoard(Piece.BLACK_KING, 0, 4, "black", 0);
        addPieceToBoard(Piece.BLACK_QUEEN, 0, 3, "black", 9);
        addPieceToBoard(Piece.BLACK_BISHOP, 0, 2, "black", 3);
        addPieceToBoard(Piece.BLACK_BISHOP, 0, 5, "black", 3);
        addPieceToBoard(Piece.BLACK_KNIGHT, 0, 6, "black", 3);
        addPieceToBoard(Piece.BLACK_KNIGHT, 0, 1, "black", 3);
        addPieceToBoard(Piece.BLACK_ROOK, 0, 0, "black", 5);
        addPieceToBoard(Piece.BLACK_ROOK, 0, 7, "black", 5);
    }

    public void addPieceToBoard(Piece label, int posX, int posY, String colour, int worth){
        Material chessMaterial = new Material(label, posX, posY, colour, worth);
        board.boardPieces.add(chessMaterial);
        chessBoard.add(chessMaterial.sceneLabel, posY, posX);

        chessMaterial.sceneLabel.setOnMouseClicked(event -> {
            System.out.println(chessMaterial.sceneLabel.getText() + " has been clicked! " +  chessMaterial.getRow() + " "  + chessMaterial.getColumn());
            if(board.whiteTurn && chessMaterial.team.equals("white") || !board.whiteTurn && chessMaterial.team.equals("black")) {
                if (event.getButton().equals(MouseButton.PRIMARY)) {
                    board.setPieceSelected(chessMaterial);
                }
                if(event.getButton().equals(MouseButton.SECONDARY)){
                    board.deselectPiece(chessMaterial);

                }
            }else{
                board.move(chessMaterial.getRow(), chessMaterial.getColumn());
            }
        });
    }

    public void setUpBoard(){

        for (int row = 0; row < 8; row++) {
            for (int col = 0; col < 8; col++) {
                Rectangle square = new Rectangle();
                square.setWidth(50);
                square.setHeight(50);
                board.setBackground(row, col, square);
                chessBoard.add(square, col, row);
                int finalCol = col;
                int finalRow = row;
                square.setOnMouseClicked(event -> {
                    if (event.getButton().equals(MouseButton.SECONDARY) && board.pieceSelected != null){
                        board.deselectPiece(board.pieceSelected);}
                    else{
                        board.move(finalRow, finalCol);
                    }
                });
            }
        }
        addPieces();
    }

    public void show() {
        stage.show();
    }
}
