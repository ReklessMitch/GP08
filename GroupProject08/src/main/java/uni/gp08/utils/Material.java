package uni.gp08.utils;

import javafx.scene.control.Label;
import uni.gp08.enums.Piece;
import uni.gp08.types.Position;

public class Material {

    Piece chessPiece;
    Label sceneLabel;
    Position position;
    int worth;
    String team;

    public Material(Piece label, int row, int column, String team, int worth) {
        this.chessPiece = label;
        this.sceneLabel = new Label(label.label);
        this.position = new Position(row, column);
        this.team = team;
        this.worth = worth;
        resetColour();
    }
    public int getWorth(){
        return worth;
    }

    public Piece getLabel() {
        return chessPiece;
    }

    public void selectedColour(){
        sceneLabel.setStyle("-fx-font-size: 30px; -fx-text-fill: red; -fx-alignment: center;");
    }

    public void resetColour(){
        if ("white".equals(team)) {
            sceneLabel.setStyle("-fx-font-size: 30px; -fx-text-fill: white; -fx-alignment: center;");
        }else{
            sceneLabel.setStyle("-fx-font-size: 30px; -fx-text-fill: black; -fx-alignment: center;");
        }
    }

    public int getRow() {
        return position.getRow();
    }

    public void setRow(int row) {
        this.position.setRow(row);
        position.rowToLetter();
    }

    public int getColumn() {
        return position.getCol();
    }

    public void setColumn(int column) {
        this.position.setCol(column);
    }

    public String getTeam() {
        return team;
    }

    @Override
    public String toString() {
        return "Material{" +
                "label=" + sceneLabel +
                ", row=" + position.getRow() +
                ", column=" + position.getCol() +
                ", worth=" + worth +
                ", team='" + team + '\'' +
                '}';
    }
}
