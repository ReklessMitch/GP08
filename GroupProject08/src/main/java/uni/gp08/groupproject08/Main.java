package uni.gp08.groupproject08;

import com.mongodb.client.*;
import javafx.application.Application;
import javafx.stage.Stage;
import uni.gp08.utils.GUI;

import org.bson.Document;


public class Main extends Application {

    public static void main(String[] args) {launch(args);}

    @Override
    public void start(Stage stage) {

        // Note will only work on ur personal IP or IP which is allowed on the console
        //ChessDB databaseConnection = new ChessDB();
        //databaseConnection.getMoves();

        GUI boardGUI = new GUI();
        boardGUI.show();

    }
}
