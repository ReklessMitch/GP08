package uni.gp08.groupproject08;

import com.mongodb.client.*;
import org.bson.Document;

import java.util.ArrayList;
import java.util.List;

public class ChessDB {
    private MongoClient mongoClient;
    private MongoDatabase database;
    private MongoCollection<Document> collection;

    public ChessDB() {
        // Connect to the MongoDB server
        mongoClient = MongoClients.create("mongodb+srv://mih71:127PharahMitch@chesstutorgp08.dkrj4id.mongodb.net/?retryWrites=true&w=majority");
        database = mongoClient.getDatabase("ChessTutorGP08");

        // Create a Collection
        collection = database.getCollection("moves");
    }

    public void storeMove(String move, String piece, String from, String to) {
        // Store data in the Collection
        Document newMove = new Document("move", move)
                .append("piece", piece)
                .append("from", from)
                .append("to", to);
        collection.insertOne(newMove);
    }

    public List<Document> getMoves() {
        List<Document> moves = new ArrayList<>();
        // Retrieve data from the Collection
        for (Document document : collection.find()) {
            moves.add(document);
        }
        return moves;
    }
}