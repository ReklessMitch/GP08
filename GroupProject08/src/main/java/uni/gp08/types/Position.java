package uni.gp08.types;

public class Position {
    int row;
    int col;
    char rowLetter;

    public Position(int row, int col) {
        this.row = row;
        this.col = col;
        rowToLetter();
    }

    public void rowToLetter(){
        char[] mapping = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'};
        rowLetter = mapping[row];
    }

    public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public void setCol(int col) {
        this.col = col;
    }

    public char getRowLetter() {
        return rowLetter;
    }
}
