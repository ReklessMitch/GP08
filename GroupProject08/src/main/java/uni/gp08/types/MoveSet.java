package uni.gp08.types;


import uni.gp08.enums.Piece;
import uni.gp08.utils.Material;

public class MoveSet {
    Material material;
    Position startPos;
    Position endPos;

    public MoveSet(Material material, Position endPos) {
        this.material = material;
        this.startPos = new Position(material.getRow(), material.getColumn());
        this.endPos = endPos;
    }

    public Piece getMaterialLabel() {
        return material.getLabel();
    }

    public Position getEndPos() {
        return endPos;
    }

    @Override
    public String toString() {
        return "MoveSet{" +
                "material=" + material.getLabel().label +
                ", startPos=" + startPos +
                ", endPos=" + endPos +
                '}';
    }
}
